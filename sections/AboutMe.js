import Container from "../components/Container";

const URL = "https://www.stefantopalovic.com/static/media/about-img.62b47e7f183d4b4e9feb.webp";

export default function AboutMe() {
    return (
        <Container id="about" light>
            <section className="flex flex-col xl:flex-row">
                <div className="basis-1/2 px-[5%] pb-3 xl:pb-0 my-auto">
                    <img src={"./about.JPG"} alt={"./about.JPG"} className="rounded-3xl" />
                </div>

                <div className="basis-1/2">
                    <section className="h-full">
                        <p className="text-red-500 text-lg font-bold pb-3">ABOUT ME</p>
                        <p className="text-2xl font-bold pb-3">A dedicated Software Engineer based in Melbourne, Australia 📍</p>
                        <p className="text-slate-700 text-lg">
                            A Software Engineer with 8 years programming experience and completed Bachelor of Software Engineering
                            (Honours) with published article. <br />
                            My skills include frontend/backend web application development using react with NodeJS backend and mobile application development. <br />
                            I am seeking to improve my skills in the field of Software Engineering and grow within the industry. <br />
                            My hobbies include programming, watching anime, gym, hanging out with friends and playing pc/console video games during my free time. <br />
                        </p>
                    </section>
                </div>
            </section>
        </Container>
    )
}
